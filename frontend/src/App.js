import logo from './logo.svg'
import './App.css'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Middle Frontend Developer course</p>
        <a
          className="App-link"
          href="https://result.school"
          target="_blank"
          rel="noopener noreferrer"
        >
          Show more
        </a>
      </header>
    </div>
  )
}

export default App
